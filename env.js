(function (window) {
  function TimezoneDetect(){
      var dtDate = new Date('1/1/' + (new Date()).getUTCFullYear());
      var intOffset = 10000; //set initial offset high so it is adjusted on the first attempt
      var intMonth;
      var intHoursUtc;
      var intHours;
      var intDaysMultiplyBy;

      //go through each month to find the lowest offset to account for DST
      for (intMonth=0;intMonth < 12;intMonth++){
          //go to the next month
          dtDate.setUTCMonth(dtDate.getUTCMonth() + 1);

          //To ignore daylight saving time look for the lowest offset.
          //Since, during DST, the clock moves forward, it'll be a bigger number.
          if (intOffset > (dtDate.getTimezoneOffset() * (-1))){
              intOffset = (dtDate.getTimezoneOffset() * (-1));
          }
      }

      return intOffset;
  }


  window.__env = window.__env || {};
  window.__env.googleMapsUrl = "https://maps.googleapis.com/maps/api/js?key=AIzaSyAfucoZC5t-bH-q38_l6qn2XeP25Kr_1vw";
  window.__env.appId = '10000000-0000-0000-0000-000000000001';
  //window.__env.apiUrl = 'http://caydev.somee.com';
  //window.__env.apiUrl = 'http://localhost:59888';
  window.__env.apiUrl = 'http://cay-laptop/fp-api'
  window.__env.oauthUrl = 'http://cay-laptop/fp-api/oauth2/token';
  window.__env.signalrUrl = 'http://cay-laptop/fp-api/signalr';

  window.__env.apiUrl = 'http://localhost:62724';
  window.__env.oauthUrl = 'http://localhost:62724/oauth2/token';
  window.__env.signalrUrl = 'http://localhost:62724/signalr';

  window.__env.apiUrl = 'http://farmingpinoy.com'
  window.__env.oauthUrl = 'http://farmingpinoy.com/oauth2/token';
  window.__env.signalrUrl = 'http://farmingpinoy.com/signalr';

  // Base url
  window.__env.baseUrl = '/';

  // Whether or not to enable debug mode
  // Setting this to false will disable console output
  window.__env.enableDebug = true;

  window.__env.timezoneOffset = TimezoneDetect();
}(this));
